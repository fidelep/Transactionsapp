import kotlinx.serialization.decodeFromString
import kotlinx.serialization.json.Json
import model.TransactionsEntity.*
import utils.StringUtils.Companion.getTemplate
import utils.StringUtils.Companion.twoDecimalsFormat
import java.io.File

val months = listOf(
    "Enero", "Febrero", "Marzo",
    "Abril", "Mayo", "Junio",
    "Julio", "Agosto", "Septiembre",
    "Octubre", "Noviembre", "Diciembre"
)

fun main(args: Array<String>) {
    val transactions = readFile("src/main/resources/raw/transactions.json")

    val byDate = transactions.groupBy { it.creationDate.substringBefore("/").toInt() }.toSortedMap()

    byDate.forEach {
        printReportByMonth(it.key, it.value)
    }
}

fun printReportByMonth(monthNumber: Int, transactions: List<Transaction>) {
    var incomeAmount = 0.00
    var expensesAmount = 0.0
    val pendingAmount = transactions.count { it.status == Status.PENDING }
    val blockedAmount = transactions.count { it.status == Status.REJECTED }

    transactions.forEach {
        if (it.status == Status.DONE) {
            when (it.operation) {
                Operation.IN -> incomeAmount += it.amount
                Operation.OUT -> expensesAmount += it.amount
            }
        }
    }

    var map = mutableMapOf<Category, Double>()
    transactions.groupBy { it.category }.forEach { category ->
        var sum = 0.0
        category.value.forEach {
            if (it.operation == Operation.OUT && it.status == Status.DONE) {
                sum += it.amount
            }
        }

        if (sum > 0.0) {
            map[category.key] = sum
        }

    }
    map = map.toList().sortedByDescending { (_, value) -> value }.toMap() as MutableMap<Category, Double>
    val expensesPercentage = StringBuilder()
    map.forEach {
        expensesPercentage.append(
            "${it.key.value}\t\t%${
                twoDecimalsFormat(
                    getPercentage(
                        it.value,
                        expensesAmount
                    )
                )
            }\n\t\t"
        )
    }

    println(
        String.format(
            getTemplate(), months[monthNumber - 1],
            pendingAmount, blockedAmount, incomeAmount, expensesAmount, expensesPercentage
        )
    )
}

fun readFile(filename: String): List<Transaction> = Json.decodeFromString(File(filename).readText())

fun getPercentage(amount: Double, top: Double): Double = (amount * 100) / top



