package model

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

class TransactionsEntity {

    @Serializable
    enum class Operation(val value: String) {
        @SerialName("in")
        IN("in"),

        @SerialName("out")
        OUT("out")
    }

    @Serializable
    enum class Category(val value: String) {
        @SerialName("Alimentacion")
        ALIMENTACION("Alimentacion"),

        @SerialName("Hogar")
        HOGAR("Hogar"),

        @SerialName("Entretenimiento")
        ENTRETENIMIENTO("Entretenimiento"),

        @SerialName("Servicios")
        SERVICIOS("Servicios"),

        @SerialName("Transferencias")
        TRANSFERENCIAS("Transferencias"),

        @SerialName("Retiros en cajero")
        RETIRO_EFECTIVO("Retiros en cajero"),

        @SerialName("Otros")
        OTROS("Otros"),

        @SerialName("Transporte")
        TRANSPORTE("Transporte")
    }

    @Serializable
    enum class Status(val value: String) {
        @SerialName("rejected")
        REJECTED("rejected"),

        @SerialName("pending")
        PENDING("pending"),

        @SerialName("done")
        DONE("done")
    }

    @Serializable
    data class Transaction(
        val uuid: Long,
        val description: String,
        val category: Category,
        val operation: Operation,
        val amount: Double,
        val status: Status,
        @SerialName("creation_date")
        val creationDate: String
    )

}