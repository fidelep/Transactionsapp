package utils

class StringUtils {
    companion object {
        fun twoDecimalsFormat(value: Double): String = String.format("%.2f", value)
        fun getTemplate(): String = """%s:
    %s transacciones pendientes
    %s bloqueadas
    
    $%.2f ingresos
    
    $%.2f gastos
        
        %s
"""
    }
}